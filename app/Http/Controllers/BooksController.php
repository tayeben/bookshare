<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Category;
use App\Models\Author;
use App\Models\Publisher;
use App\Models\Book;
use App\Models\BookAuthor;
use App\Models\BookRequest;


class BooksController extends Controller
{
  
    public function index()
    {
        $books = Book::orderBy('id', 'desc')->where('is_approved', 1)->paginate(10);
        return view('frontend.pages.books.index', compact('books'));

    }


    public function search(Request $request)
    {
        $searched = $request->s;
        if (empty($searched)) {
            return $this->index();
        }

        $books = Book::orderBy('id', 'desc')->where('is_approved', 1)
        ->where('title', 'like', '%'.$searched.'%')
        ->orWhere('description', 'like', '%'.$searched.'%')
        ->paginate(10);

        foreach ($books as $book) {
            $book->increment('total_search');
        }

        return view('frontend.pages.books.index', compact('books', 'searched'));
    }


    public function advanceSearch(Request $request)
    {
        $query = Book::query()->where('is_approved', 1);
    
        // Extract search parameters from the request
        $searched = $request->input('t');
        $searched_publisher = $request->input('p');
        $searched_category = $request->input('c');
    
        // Apply filters based on the presence of search parameters
        if (!empty($searched)) {
            $query->where(function ($subQuery) use ($searched) {
                $subQuery->where('title', 'like', "%$searched%")
                         ->orWhere('description', 'like', "%$searched%");
            });
        }
    
        if (!empty($searched_publisher)) {
            $query->where('publisher_id', $searched_publisher);
        }
    
        if (!empty($searched_category)) {
            $query->where('category_id', $searched_category);
        }
    
        // Retrieve paginated search results
        $books = $query->orderBy('id', 'desc')->paginate(10);
    
        // Increment total_search for each book
        $books->each(function ($book) {
            $book->increment('total_search');
        });
    
        // Pass the search results and search keyword to the view
        return view('frontend.pages.books.index', compact('books', 'searched'));
    }
    




    public function show($slug)
    {
    	$book = Book::where('slug', $slug)->first();
    	
    	if (!is_null($book)) {
    		return view('frontend.pages.books.show', compact('book'));
    	}
    	return redirect()->route('index');
    }

	public function create()
    {
    	$categories = Category::all();
        $publishers = Publisher::all();
        $authors = Author::all();
        $books = Book::where('is_approved', 1)->get();

        return view('frontend.pages.books.create', compact('categories', 'publishers', 'authors', 'books'));
    }

    public function store(Request $request)
    {
    	if (!Auth::check()) {
    		abort(403, 'Unauthorized action');
    	}

        $request->validate([
            'title' => 'required|max:50',
            'category_id' => 'required',
            'publisher_id' => 'required',
            'slug' => 'nullable|unique:books',
            'description' => 'nullable',
            'image' => 'required|image|max:2048',
            'quantity' => 'required|numeric|min:1',
            'market_price' => 'required|numeric',
            'lending_price' => 'required|numeric|min:50',
            'selling_price' => 'required|numeric|min:80'
        ],
        [
            'title.required' => 'Please give book title',
            'image.max' => 'Image size can not be greater than 2MB'
        ]);
        $book = new Book();
        $book->title = $request->title;
        if (empty($request->slug)) {
            $book->slug = Str::slug($request->title);
        }else{
            $book->slug = $request->slug;
        }
        
        $book->category_id = $request->category_id;
        $book->publisher_id = $request->publisher_id;
        $book->publish_year = $request->publish_year;
        $book->description = $request->description;
        $book->user_id = Auth::id();
        $book->is_approved = 0;
        $book->isbn = $request->isbn;
        $book->quantity = $request->quantity;
        $book->market_price = $request->market_price;
        $book->lending_price = $request->lending_price;
        $book->selling_price = $request->selling_price;

        $book->translator_id = $request->translator_id;
        $book->save();

        // Image Upload
        if ($request->image) {
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $name = time().'-'.$book->id.'.'.$ext;
            $path = "images/books";
            $file->move($path, $name);
            $book->image = $name;
            $book->save();
        }

        // Book Authors
        foreach ($request->author_ids as $id) {
            $book_author = new BookAuthor();
            $book_author->book_id = $book->id;
            $book_author->author_id = $id;
            $book_author->save();
        }
        

        session()->flash('success', 'Book has been created !!');
        return redirect()->route('users.dashboard.books');
    }

}
