    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('admin.index') }}">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-file"></i>
            </div>
            <div class="sidebar-brand-text mx-3">Admin Panel</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('admin.index') }}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Interface
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-file"></i>
                <span>Books</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Manage Books & All</h6>
                    <a class="collapse-item" href="{{ route('admin.books.index') }}">All Books</a>
                    <a class="collapse-item" href="{{ route('admin.books.unapproved') }}">
                        Unapproved Books
                        <span class="badge badge-warning">
                            {{ count(App\Models\Book::where('is_approved', 0)->get()) }}
                        </span>
                    </a>
                    <a class="collapse-item" href="{{ route('admin.books.approved') }}">
                        Approved Books
                        <span class="badge badge-success">
                            {{ count(App\Models\Book::where('is_approved', 1)->get()) }}
                        </span>
                    </a>
                    {{-- <a class="collapse-item" href="{{ route('admin.books.create') }}">Create Book</a> --}}
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.categories.index') }}">
                <i class="fas fa-fw fa-th"></i>
                <span>Categories</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.authors.index') }}">
                <i class="fas fa-fw fa-user"></i>
                <span>Authors</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.publishers.index') }}">
                <i class="fas fa-fw fa-bell"></i>
                <span>Publishers</span></a>
        </li>


        <hr class="sidebar-divider d-none d-md-block">




        {{-- <li class="nav-item">
          <a class="nav-link" href="{{ route('admin.users.index') }}">
              <i class="fas fa-fw fa-bell"></i>
              <span>Users</span></a>
      </li>
      <hr class="sidebar-divider d-none d-md-block"> --}}

        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-sign-out-alt"></i>
                <span>Logout</span></a>
            </a>
        </li>


<br>
<br>
<br>


        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->
