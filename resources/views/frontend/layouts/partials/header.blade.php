<header class="header" style="background-color: #f8f9fa;">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="{{ route('index') }}">
                <img src="{{ asset('images/logo.jpg') }}" class="logo-image" alt="Logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('index') }}">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('books.index') }}">Recent Books</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle pointer" href="#" id="dropdownMenuButton" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-book"></i> My Books
                        </a>
                        <div class="dropdown-menu dropdown-menu-right animated fadeInDown" aria-labelledby="dropdownMenuButton">
                            @if (Auth::check())
                                
                                <a class="dropdown-item" href="{{ route('books.upload') }}">Upload Now</a>
                                <a class="dropdown-item" href="{{ route('users.dashboard.books') }}">My Uploaded Books</a>
                                <a class="dropdown-item" href="{{ route('books.order.list') }}">My Ordered Books</a>
                            @else
                                <a class="dropdown-item" href="{{ route('login') }}">Login For Your Books</a>
                                <a class="dropdown-item" href="{{ route('books.index') }}">Recent Uploaded Books</a>
                            @endif
                        </div>
                    </li>
                    
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle pointer" href="#" id="dropdownMenuButton" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user"></i> My Account
                        </a>
                        <div class="dropdown-menu dropdown-menu-right animated fadeInDown" aria-labelledby="dropdownMenuButton">
                            @if (Auth::check())
                                
                                <a class="dropdown-item" href="{{ route('users.dashboard') }}">Dashboard</a>
                                <a class="dropdown-item" href="{{ route('users.dashboard.books') }}">My Uploaded Books</a>
                                <a class="dropdown-item" href="{{ route('books.order.list') }}">My Ordered Books</a>

                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            @else
                                <a class="dropdown-item" href="{{ route('login') }}">Sign In</a>
                                <a class="dropdown-item" href="{{ route('register') }}">Sign Up</a>
                            @endif
                        </div>
                    </li>


                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <form class="form-inline my-2 my-lg-0" action="{{ route('books.search') }}" method="get">
                            <input class="form-control mr-sm-2 search-form" type="text" placeholder="Search"
                                aria-label="Search" name="s" value="{{ isset($searched) ? $searched : '' }}">
                            <button class="btn btn-success my-2 my-sm-0 search-button" type="submit"><i
                                    class="fa fa-search"></i></button>
                        </form>
                    </li>
                    


                </ul>
            </div>
        </nav>
    </div>
</header>
