<style>
    .card {
        transition: transform 0.3s;
        height: 100%;
        display: flex;
        flex-direction: column;
        padding: 10px; /* Add padding for gap */
    }

    .card:hover {
        transform: translateY(-5px);
    }

    .card-title {
        font-size: 1.2rem;
        margin-bottom: 0.5rem;
    }

    .card-text {
        color: #6c757d;
        flex-grow: 1;
    }

    .card-footer {
        background-color: #f8f9fa;
        border-top: none;
    }

    .card-footer .btn {
        font-size: 0.8rem;
    }

    .card-footer .btn:not(:last-child) {
        margin-right: 5px;
    }

    .card-body {
        padding: 15px;
    }
</style>

<div class="row">
    @foreach ($books as $book)
    <div class="col-md-4 mb-4">
        <div class="card shadow-sm">
            <img src="{{ asset('images/books/'.$book->image) }}" class="card-img-top" alt="{{ $book->title }}">
            <div class="card-body">
                <h5 class="card-title">{{ $book->title }}</h5>
                <p class="card-text">
                    Uploaded by: <a href="{{ route('users.profile', $book->user->username) }}">{{ $book->user->name }}</a>
                </p>
            </div>
            <div class="card-footer">
                @if (Route::is('users.dashboard.books'))
                <div class="btn-group">
                    <a href="{{ route('books.show', $book->slug) }}" class="btn btn-sm btn-primary">View</a>
                    <a href="{{ route('users.dashboard.books.edit', $book->slug) }}" class="btn btn-sm btn-success">Edit</a>
                    <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal{{ $book->id }}">Delete</button>
                </div>
                @else
                <div class="d-flex justify-content-between align-items-center">
                    <a href="{{ route('books.show', $book->slug) }}" class="btn btn-sm btn-primary">View</a>
                    <small class="text-muted">{{ $book->created_at->diffForHumans() }}</small>
                </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal{{ $book->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('users.dashboard.books.delete', $book->id) }}" method="post">
                        @csrf

                        <div>{{ $book->title }} will be deleted!!</div>

                        <div class="mt-4">
                            <button type="submit" class="btn btn-primary">Ok, Confirm</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Modal -->
    @endforeach
</div>
