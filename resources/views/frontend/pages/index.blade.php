@extends('frontend.layouts.app')


@section('content')


<style>
  .overlay {
  background-color: rgba(0, 0, 0, 0.5); /* Adjust the opacity as needed */
  padding: 10px 20px;
  border-radius: 5px;
}

.form-group:hover {
        transform: translateY(-5px);
    }


.advance-search {
  background-color: #f9f9f9;
  padding: 30px 0;
}

.advance-search h3 {
  color: #333;
}

.advance-search form .form-group label {
  font-weight: bold;
}

.advance-search form .btn-primary {
  background-color: #007bff;
  border-color: #007bff;
}

.advance-search form .btn-primary:hover {
  background-color: #0056b3;
  border-color: #0056b3;
}

.advance-search form .btn-primary:focus {
  box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.5);
}


  </style>

<div class="main-content">
 <!-- Carousel -->
 <div id="carouselExampleIndicators" class="carousel slide main-slider" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="{{ asset('images/sliders/slider1.png') }}" class="d-block w-100">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="">Welcome to our Book Sharing Platform</h1>
        <br>
        <br>
        <br>
        <p class="overlay">
          Here you can borrow or buy second hand or new books. You can upload books for sell or give to others for contacted time and earn some if you want.
        </p>
        <br>
        <br>
      </div>
    </div>
    <div class="carousel-item">
      <img src="{{ asset('images/sliders/slider2.png') }}" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h2 class="">Start Share your Books at our Book Sharing Platform</h2>
        <br>
        <br>
        <br>
        <p class="overlay">
          “Give a man a fish and you feed him for a day; teach a man to fish and you feed him for a lifetime; give a man a book and you feed his mind for a lifetime.” - Chinese Proverb
        </p>
        <br>
        <br>
      </div>
    </div>
    <div class="carousel-item">
      <img src="{{ asset('images/sliders/slider3.jpg') }}" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h2 class="">Start Borrow or Buy needed Books from our Book Sharing Platform</h2>
        <br>
        <br>
        <br>
        <p class="overlay">
          "A reader lives a thousand lives before he dies. The man who never reads lives only one." - George R.R. Martin
        </p>
        <p class="overlay">
          "Books are the quietest and most constant of friends; they are the most accessible and wisest of counselors, and the most patient of teachers." - Charles W. Eliot
        </p>
        <p class="overlay">
          "Books are the plane, and the train, and the road. They are the destination, and the journey. They are home." - Anna Quindlen
        </p>
        <br>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<!-- Carousel -->



  <div class="top-body pt-4 pb-4">
    <div class="container">
      
      @if(Session::has('status'))
        <div class="alert alert-success">
            <p>
                {{ Session::get('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </p>

        </div>
    @endif

      <div class="row">

       
      </div>
    </div>
  </div> <!-- End Top Body Links -->





  <!-- Start of advanceSearch -->

  <div class="advance-search">
    <div class="container">
      <h3 class="text-center mb-4">Advanced Search</h3>
      <form action="{{ route('books.searched.advance') }}" method="post">
        @csrf
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="bookTitle">Book Title/Description</label>
              <input type="text" class="form-control" id="bookTitle" placeholder="Enter book title or description" name="t">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="publication">Publication</label>
              <select class="form-control" id="publication" name="p"> 
                <option value="">Select a publisher</option>
                @foreach ($publishers as $pub)
                  <option value="{{ $pub->id }}">{{ $pub->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="category">Book Category</label>
              <select class="form-control" id="category" name="c"> 
                <option value="">Select a category</option>
                @foreach ($categories as $cat)
                  <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-md-12 text-center">
            <button type="submit" class="btn btn-primary btn-lg">
              <i class="fa fa-search"></i> Search
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
  


<!-- End of advanceSearch -->




  <div class="book-list-sidebar">
    <div class="container">
      <div class="row">

        <div class="col-md-9">
          <h3>Recent Uploaded Books</h3>

          @include('frontend.pages.books.partials.list')

          <div class="books-pagination mt-5">
            {{ $books->links() }}
          </div>

        </div> <!-- Book List -->

        <div class="col-md-3">
          <div class="widget">
            <h5 class="mb-2 border-bottom pb-3">
              Categories
            </h5>

            @include('frontend.pages.books.partials.category-sidebar')

          </div> <!-- Single Widget -->

        </div> <!-- Sidebar -->

      </div>
    </div>
  </div>
</div>

@endsection